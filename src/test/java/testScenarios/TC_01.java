package testScenarios;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import commonUtilities.InitialiseBrowser;
import pageElements.HomePageElements;

@Listeners(commonUtilities.Listeners.class)
public class TC_01{
	public InitialiseBrowser bc = new InitialiseBrowser();

	@Test
	public void testcaseOne() throws IOException, InterruptedException {
		bc.CalBrowser();
		Thread.sleep(7000);
	}

	@Test
	public void zebra() {
		HomePageElements home = new HomePageElements();
		bc.driver.findElement(home.Login_Btn).click();
		Assert.assertEquals(22, 23);
	}
}
