package testScenarios;

import java.net.MalformedURLException;
import java.net.URL;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class Grid_SetUp {
	public static WebDriver driver;

	public static void main(String[] args) throws MalformedURLException, InterruptedException {
		String URL = "https://constructionspecialties.egnyte.com/app/index.do#storage/files/1/Shared/ORACLE/Orders/0001/1030";
		String Node = "http://172.26.201.84:5555/wd/hub";
		DesiredCapabilities cap = DesiredCapabilities.internetExplorer();
		driver = new RemoteWebDriver(new URL(Node), cap);
		driver.navigate().to(URL);
		driver.manage().window().maximize();
		Thread.sleep(5000);
		driver.quit();
	}
}
