package commonUtilities;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Iterator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class WritingDataIntoExcel {
	static XSSFWorkbook excelWbook;
    static XSSFSheet sheet;
    Iterator<Row> rowIterator;
    XSSFRow row;
    XSSFCell cell;
    		
public static void writeexcel(String fileName, String SheetName, String result, int colNum, int rowNum){
            try
                {
            	FileInputStream ExcelFile =  new FileInputStream(new File(System.getProperty("user.dir")+"/src\\test\\java\\testScenarios\\"+ fileName));
				
				//Access the required test data sheet
				excelWbook= new XSSFWorkbook(ExcelFile);
		
                 sheet = excelWbook.getSheet(SheetName);
                    // Check if the excelWbook is empty or not
                    int numOfSheets=excelWbook.getNumberOfSheets();
                    if (numOfSheets!= 0) {
                        for (int i = 0; i < excelWbook.getNumberOfSheets(); i++) {
                           if (excelWbook.getSheetName(i).equals(SheetName)) {
                                sheet = excelWbook.getSheet(SheetName);
                                break;
                            } 
                             if(i==numOfSheets) {
                             sheet = excelWbook.createSheet(SheetName);
                             break;
                            }
                        }
                    }
                    else {
                        // Create new sheet to the excelWbook if empty
                        sheet = excelWbook.createSheet(SheetName);
                    }
                     
                     XSSFRow row;
                     if(sheet.getRow(rowNum)==null) {
                    	 row=sheet.createRow(rowNum);
                    	 Cell cell=row.createCell(colNum);
                         cell.setCellValue(result);
                     }else {
                    	 row=sheet.getRow(rowNum);
                    	 Cell cell=row.createCell(colNum);
                         cell.setCellValue(result);
                     }
                    
                     FileOutputStream out=new FileOutputStream(new File(System.getProperty("user.dir")+"/src\\test\\java\\testScenarios\\"+ fileName));
                     excelWbook.write(out);
                     out.close();
                     System.out.println("Written successfully on excel file");      

                }
                catch (Exception e)
                {
                System.out.println("Driver entered into Catch block of writeExcel() method");
                    e.printStackTrace();
                }
        }
}
