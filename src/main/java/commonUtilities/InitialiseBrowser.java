package commonUtilities;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.testng.annotations.AfterTest;
import commonUtilities.ReadConfig;

import io.github.bonigarcia.wdm.WebDriverManager;

public class InitialiseBrowser{
	ReadConfig readConfig = new ReadConfig();
	public WebDriver driver;
	Logger log= Logger.getLogger(InitialiseBrowser.class);
	String browser=String.valueOf(readConfig.getBrowser());
	public void CalBrowser() throws IOException {
		switch (browser) {
		case "Chrome":
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver();
			log.info("launching chrome broswer");
			System.out.println("------------------------------------ Chrome Browser initiated---------------------------------------");
			getURL_Maxmise();
			break;
		case "FireFox":
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
			log.info("launching firefox broswer");
			System.out.println("------------------------------------ FireFox Browser initiated---------------------------------------");
			getURL_Maxmise();
			break;
		case "InternetExplorer":
			 WebDriverManager.iedriver().setup();
			 driver = new InternetExplorerDriver();
			 System.out.println("------------------------------------ InternetExplorer Browser initiated---------------------------------------");
			 break;
		case "Opera":
			WebDriverManager.operadriver().setup();
			driver = new OperaDriver();
			System.out.println("------------------------------------ Opera Browser initiated---------------------------------------");
			break;
		case "Edge":
			WebDriverManager.edgedriver().setup();
			driver = new EdgeDriver();
			System.out.println("------------------------------------ Edge Browser initiated---------------------------------------");
			break;
		default:
			System.out.println("No Browser initialized");
			break;
		}
	}
	private void getURL_Maxmise() {
		driver.get(readConfig.getApplicationURL());
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(5000, TimeUnit.MILLISECONDS);
		driver.manage().window().maximize();
	}
	
	
	@AfterTest
	public void tearDown() {
		if(driver != null)
		{
			System.out.println("------------------------------------ Browser Closing  ---------------------------------------");
			driver.quit();
			driver = null;
		}
	}
}
