package commonUtilities;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class ReadConfig {
	static Properties pro;
	public ReadConfig() {
	File src = new File(System.getProperty("user.dir")+"/src/main/java/configurator/Config.properties");
	
	try {
		FileInputStream fis= new FileInputStream(src);
		pro = new Properties();
		pro.load(fis);
		}
	catch(Exception e) {
		System.out.println("Exception is  "+e.getMessage());
	}
	}
	public String getApplicationURL()
	{
		String url= pro.getProperty("URL");
		return url;
	}
	
	public String getBrowser()
	{
		String browser= pro.getProperty("BROWSER");
		return browser;
	}
	public String getScreenShootFlag() {
		String screenshot = pro.getProperty("SCREENSHOT");
		return screenshot;
	}
}
