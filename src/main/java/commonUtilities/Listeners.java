package commonUtilities;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class Listeners implements ITestListener{

	public void onTestStart(ITestResult result) {
		System.out.println("The test is Started "+ result.getName());		
	}

	public void onTestSuccess(ITestResult result) {
		System.out.println("The test is pass "+ result.getName());
		
	}

	public void onTestFailure(ITestResult result) {
		/*
		File file = new File(System.getProperty("user.dir") + "\\src\\main\\java\\com\\CS\\ScreenShots\\");
		file.mkdir();
		TakesScreenshot scrShot =((TakesScreenshot)WebDriver);
		File screenshotFile = scrShot.getScreenshotAs(OutputType.FILE);
			try {
				Files.copy(screenshotFile, new File(file + "\\" + result.getName() + ".png"));
			} catch (IOException e) {
				e.printStackTrace();
			}
			*/
	}

	public void onTestSkipped(ITestResult result) {
		System.out.println("The test is Skip "+ result.getName());
		
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		System.out.println("The test is Failure "+ result.getName());
		
	}

	public void onStart(ITestContext context) {
		System.out.println("The test is On Start "+ context.getOutputDirectory());
		
	}

	public void onFinish(ITestContext context) {
		System.out.println("The test is On Finish" + context.getOutputDirectory());
		
	}
}
